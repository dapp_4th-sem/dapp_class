const SubjectList = artifacts.require('SubjectList')

// creating the contract where we can code for test any function
contract('SubjectList', (account) => {
    beforeEach(async () => {                 // beforeEach function can be run before the test case
        this.subjectList = await SubjectList.deployed()  // retrived the instance of smart contracts
    })

    it('deploys successfully', async () => {    // it is also function mocha testy framework 
        // and it used to define the new test case
        const address = await this.subjectList.address
        isValidAddress(address)
    })
  
   // Testing the content in the contract
   it('test adding subject', async () => {
    return SubjectList.deployed().then ((instance) => {
        s = instance;
        subjectCode = "CSB201";
        return s.createSubject(subjectCode, "Developing Decentralized application");
    }).then((transaction) => {
        isValidAddress(transaction.tx)
        isValidAddress(transaction.receipt.blockHash);
        return s.subjectsCount()
    }).then((count) => {
        assert.equal(count,1)
        return s.subjects(1);
    }).then((subject) => {
        assert.equal(subject.code, subjectCode)
    })
})

it('test finding subject', async() =>{
    return SubjectList.deployed().then(async(instance) => {
        s = instance;
        code = 2;
        s.createSubject(code ++, "python");
        s.createSubject(code ++ , "Dzongkha");
        return s.subjectsCount().then(async (count) => {
            assert.equal(count, 3)
        return s.findSubject(3).then(async(subject) =>{
            assert.equal(subject.subject, "Dzongkha")
        })    
        })
    })
})
it('test mark retired subject', async() =>{
    return SubjectList.deployed().then(async (instance) =>{
        s= instance;
        return s.findSubject(2).then(async(osubject) => {
            assert.equal(osubject.subject, "python")
            assert.equal(osubject.retired, false)
            return s.markRetired(2).then(async(transcation) =>{
                return s.findSubject(2).then(async (nsubject) =>{
                    assert.equal(nsubject.subject, "python")
                    assert.equal(nsubject.retired, true)
                })
            })
        })
    })
})
it('test the updating of subject', async() =>{
    return SubjectList.deployed().then(async(instance) =>{
        s = instance;
        s.createSubject("IT101","programming");
        s.updatedSubject(7,"IT102","programming langauge");
        return s.findSubject(7).then(async (nsubject) => {
            assert.equal(nsubject.code,"IT102",)
            assert.equal(nsubject.subject,"programming langauge")
        })
            
    })
})
})
function isValidAddress(address){
    assert.notEqual(address, 0x0) // assert.notEqual is also function that takes parameter acutal and expected values
    assert.notEqual(address,'')
    assert.notEqual(address,null)
    assert.notEqual(address,undefined)
}