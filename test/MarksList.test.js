var MarksList = artifacts.require("MarksList");
const SubjectList = artifacts.require('SubjectList')
const StudentList = artifacts.require('StudentList')

contract('MarksList', (account) => {
    // make sure contract is deployed
    beforeEach(async () => {
        this.MarksList = await MarksList.deployed()
    })

    it('Markslist, studentList,subjectLsit test', async () => {
        assert.equal(MarksList._json.contractName, "MarksList",
            "markslist contract is correct"
        )
        subjectcontractname = SubjectList._json.contractName;
        assert.equal(subjectcontractname, "SubjectList")
        studentcontractname = StudentList._json.contractName;
        assert.equal(studentcontractname, "StudentList")
    })

    // testing the content in the contract
    it('adding student test', async () => {
        return StudentList.deployed().then(async (instance) => {
            s = instance;
            const names = ["PuruShotam", "Dorji Phuntsho"];
            studentCid = 1;
            for (let i = 0; i < names.length; i++) {
                s.createStudent(studentCid, names[i]);
                studentCid++;
            }
        })
    })
    it('adding subject test', async () => {
        return SubjectList.deployed().then(async (instance) => {
            s = instance;
            const subjects = ["programming_Blockchain", "Dapp_Development"];
            const codes = ["CSB203", "CSB202"];
            id = 1;
            for (let i = 0; i < subjects.length; i++) {
                s.createSubject(codes[i], subjects[i]);
                id++;

            }

        })
    })
    it('testing marks adding', async () => {
        return MarksList.deployed().then((instance) => {
            m = instance;
            return m.addMarks(2, "CSF101", 1).then(async (transaction) => {
                return m.marksCount().then(async (count) => {
                    assert.equal(count, 1)
                    return m.findMarks(2, "CSF101").then(async (marks) => {
                        assert.equal(marks.code, "CSF101")
                        assert.equal(marks.grades, 1)
                    })
                })
            })
        })
    })
     
    // Define the grades as an object
    const Grades = {
        A: 1,
        B: 2,
        C: 3,
        D: 4,
        F: 5
      };
  
      it("should update marks", async () => {
        const s = await MarksList.deployed();
        await s.updateMarks(1, "student1", Grades.A);
      
        const updatedMarks = await s.marks(1, "student1");
        assert.equal(updatedMarks.grades, Grades.A, "Grade not updated correctly");
      });
})

function isValidAddress(address) {
    assert.notEqual(address, 0x0)
    assert.notEqual(address, '')
    assert.notEqual(address, null)
    assert.notEqual(address, undefined)
}