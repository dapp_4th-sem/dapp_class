// Import the StudentList smart contract
const StudentList = artifacts.require('StudentList')

//Use the contract to write all test
//Variable: account => all accounts in blockchain
contract('StudentList', (account) =>{
    beforeEach(async () => {
        this.StudentList = await StudentList.deployed()
    })

    //Testing the deployed student contract
    it('deploys successfully', async () => {
        //Get the address which the student object is stored 
        const address = await this.StudentList.address
        //Test for valid address
        isValidAddress(address)
    })

    it('test adding students', async () => {
        return StudentList.deployed().then ((instance) => {
            s = instance;
            studentCid = 1;
            return s.createStudent(studentCid, "Maya");
        }).then((transaction) => {
            isValidAddress(transaction.tx)
            isValidAddress(transaction.receipt.blockHash);
            return s.studentsCount()
        }).then((count) => {
            assert.equal(count, 2)
            return s.students(2);
        }).then((student) => {
            assert.equal(student.cid, studentCid)
        })
    })

    it('test finding students', async () => {
        return StudentList.deployed().then (async (instance) => {
            s = instance;
            studentCid = 2;
            return s.createStudent(studentCid++, "Tshering Yangzom").then(async (tx) =>{
                return s.createStudent(studentCid++, "Dorji Tshering").then(async (tx) => {
                    return s.createStudent(studentCid++, "Chimi Dema").then(async (tx) => {
                        return s.createStudent(studentCid++, "Tashi Phuntsho").then(async (tx) => {
                            return s.createStudent(studentCid++, "Samten Zangmo").then(async (tx) => {
                                return s.createStudent(studentCid++, "Sonam Chophel").then(async (tx) =>{
                                    return s.studentsCount().then(async (count) => {
                                        assert.equal(count,8)
                                    return s.findStudent(5).then(async(student) => {
                                        assert.equal(student.name, "Chimi Dema")
                                    })
                                    })
                                })
                            })
                        })
                    })
                })
            })
        })
    })

    it('test mark graduate students', async() => {
        return StudentList.deployed().then(async (instance) => {
            s = instance;
            return s.findStudent(2).then(async (ostudent) => {
                assert.equal(ostudent.name, "Maya")
                assert.equal(ostudent.graduated, false)
                return s.markGraduated(2).then(async (transaction) => {
                    return s.findStudent(2).then(async (nstudent) => {
                        assert.equal(nstudent.name, "Maya")
                        assert.equal(nstudent.graduated, true)
                        return
                    })
                })
            })
        })
    })
    

})

//This function check if the address is valid 
function isValidAddress(address){
    assert.notEqual(address, 0x0)
    assert.notEqual(address, '')
    assert.notEqual(address, null)
    assert.notEqual(address, undefined)

}