
export const MARK_LIST_ADDRESS = "0xe136561B3a1be0e7220Db91301B0ec744D82860A"

export const MARK_LIST_ABI =  [

  {
    "inputs": [],
    "stateMutability": "nonpayable",
    "type": "constructor"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "indexed": true,
        "internalType": "uint256",
        "name": "cid",
        "type": "uint256"
      },
      {
        "indexed": true,
        "internalType": "string",
        "name": "code",
        "type": "string"
      },
      {
        "indexed": false,
        "internalType": "enum MarksList.Grades",
        "name": "grades",
        "type": "uint8"
      }
    ],
    "name": "addMarksEvent",
    "type": "event"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "indexed": false,
        "internalType": "uint256",
        "name": "_cid",
        "type": "uint256"
      },
      {
        "indexed": false,
        "internalType": "string",
        "name": "_code",
        "type": "string"
      },
      {
        "indexed": false,
        "internalType": "enum MarksList.Grades",
        "name": "_newgrades",
        "type": "uint8"
      }
    ],
    "name": "updateMarksEvent",
    "type": "event"
  },
  {
    "inputs": [
      {
        "internalType": "uint256",
        "name": "",
        "type": "uint256"
      },
      {
        "internalType": "string",
        "name": "",
        "type": "string"
      }
    ],
    "name": "marks",
    "outputs": [
      {
        "internalType": "uint256",
        "name": "cid",
        "type": "uint256"
      },
      {
        "internalType": "string",
        "name": "code",
        "type": "string"
      },
      {
        "internalType": "enum MarksList.Grades",
        "name": "grades",
        "type": "uint8"
      }
    ],
    "stateMutability": "view",
    "type": "function",
    "constant": true
  },
  {
    "inputs": [],
    "name": "marksCount",
    "outputs": [
      {
        "internalType": "uint256",
        "name": "",
        "type": "uint256"
      }
    ],
    "stateMutability": "view",
    "type": "function",
    "constant": true
  },
  {
    "inputs": [
      {
        "internalType": "uint256",
        "name": "_cid",
        "type": "uint256"
      },
      {
        "internalType": "string",
        "name": "_code",
        "type": "string"
      },
      {
        "internalType": "enum MarksList.Grades",
        "name": "_grades",
        "type": "uint8"
      }
    ],
    "name": "addMarks",
    "outputs": [],
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "inputs": [
      {
        "internalType": "uint256",
        "name": "_cid",
        "type": "uint256"
      },
      {
        "internalType": "string",
        "name": "_code",
        "type": "string"
      }
    ],
    "name": "findMarks",
    "outputs": [
      {
        "components": [
          {
            "internalType": "uint256",
            "name": "cid",
            "type": "uint256"
          },
          {
            "internalType": "string",
            "name": "code",
            "type": "string"
          },
          {
            "internalType": "enum MarksList.Grades",
            "name": "grades",
            "type": "uint8"
          }
        ],
        "internalType": "struct MarksList.Marks",
        "name": "",
        "type": "tuple"
      }
    ],
    "stateMutability": "view",
    "type": "function",
    "constant": true
  },
  {
    "inputs": [
      {
        "internalType": "uint256",
        "name": "_cid",
        "type": "uint256"
      },
      {
        "internalType": "string",
        "name": "_code",
        "type": "string"
      },
      {
        "internalType": "enum MarksList.Grades",
        "name": "_newgrade",
        "type": "uint8"
      }
    ],
    "name": "updateMarks",
    "outputs": [
      {
        "components": [
          {
            "internalType": "uint256",
            "name": "cid",
            "type": "uint256"
          },
          {
            "internalType": "string",
            "name": "code",
            "type": "string"
          },
          {
            "internalType": "enum MarksList.Grades",
            "name": "grades",
            "type": "uint8"
          }
        ],
        "internalType": "struct MarksList.Marks",
        "name": "",
        "type": "tuple"
      }
    ],
    "stateMutability": "nonpayable",
    "type": "function"
  }
]