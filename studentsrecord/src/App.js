import React, {Component} from 'react';
import Web3 from 'web3';
import './App.css';
import StudentList from './components/StudentList';
import SubjectList from './components/SubjectList';
import MarkList from './components/MarkList';
import FindMarks from './components/FindMarks'
import { STUDENT_LIST_ABI, STUDENT_LIST_ADDRESS } from './abi/config_studentList';
import { SUBJECT_LIST_ABI, SUBJECT_LIST_ADDRESS } from './abi/config_subjectList';
import { MARK_LIST_ABI, MARK_LIST_ADDRESS} from './abi/config_markList';

class App extends Component{
  componentDidMount(){
    if(!window.ethereum)
      throw new Error("No crypto wallet found. please install it.");
    window.ethereum.send("eth_requestAccounts");
    this.loadBlockchainData()
   
  }
  async loadBlockchainData(){
    const web3 = new Web3(Web3.givenProvider || "https://sepolia.infura.io/v3/7a2c6923bf3d4a3d8be7baa02df2cc6b")
    const accounts = await web3.eth.getAccounts()
    this.setState({account:accounts[0]})

    //studentList
    const studentList = new web3.eth.Contract(
      STUDENT_LIST_ABI, STUDENT_LIST_ADDRESS)
    this.setState({studentList})
    const studentCount = await studentList.methods.studentsCount().call()
    this.setState({studentCount})
    this.setState.students = []
    for (var i = 1; i <= studentCount; i++) {
      const student = await studentList.methods.students(i).call();
      this.setState({
        students: [...this.state.students, student]
      })
    }

    //SubjectList
    const subjectList = new web3.eth.Contract(SUBJECT_LIST_ABI, SUBJECT_LIST_ADDRESS);
    this.setState({ subjectList });

    const subjectCount = await subjectList.methods.subjectsCount().call();
    this.setState({ subjectCount });

    const subjects = [];
    for (var j = 1; j <= subjectCount; j++) {
      const subject = await subjectList.methods.subjects(j).call();
      subjects.push(subject);
    }

    this.setState({ subjects });

    //MarkList
    const MarkList = new web3.eth.Contract(
      MARK_LIST_ABI, MARK_LIST_ADDRESS)
    this.setState({MarkList})
    const marksCount = await MarkList.methods.marksCount().call()
    this.setState({marksCount})
    // this.state.marks = []
    // for (var k = 1; k<= marksCount;k++){
    //   const Mark = await marksCount.methods.marks(k).call()
    //   this.setState({
    //     marks: [...this.state.marks, Mark]
    //   })
    // }
  }
  
  constructor(props){
    super(props)
    this.state = {
      account: '',
      studentCount: 0,
      students: [],
      loading:true,
      update:false,
      studentObj:{},
      updateCID:0,
      subjectCount:0,
      subjects:[],
      subjectObj:{},
      updateCode:0,
      marksCount:0,
      
    }
    this.createStudent = this.createStudent.bind(this)
    this.updateName = this.updateName.bind(this)
    this.markGraduated = this.markGraduated.bind(this)
    this.createSubject = this.createSubject.bind(this)
    this.updateSubject =this.updateSubject.bind(this)
    this.markRetired = this.markRetired.bind(this)
    this.createMarks = this.createMarks.bind(this)
    this.findMarks = this.findMarks.bind(this)
  }

  //******STUDENTLIST*****

  createStudent(cid,name){
    // alert("inside app.js",cid)
    this.setState({loading:true})
    this.state.studentList.methods.createStudent(cid,name)
    .send({from:this.state.account})
    .once('receipt',(receipt)=>{
      this.setState({loading:false})
      this.loadBlockchainData()
    })
  }
  updateName(id,name){
    // alert(id+name)
    this.setState({update:true})
    this.state.studentList.methods.updateStudent(id,name)
    .send({from:this.state.account})
    .once('receipt',(receipt)=>{
      this.setState({loading:false})
      this.setState({update:false})
      this.loadBlockchainData()
    })
    this.setState({loading:false})
    this.setState({update:false})
    // alert(student._id)
  }
  markGraduated(cid) {
    this.setState({loading:true})
    this.state.studentList.methods.markGraduated(cid)
    .send({from: this.state.account})
    .once('receipt', (receipt) => {
      this.setState({loading:false})
      this.loadBlockchainData()
    })
  }


    //******SUBJECTLIST*****
  createSubject(code,subject){
    // alert("inside app.js",cid)
    this.setState({loading:true})
    this.state.subjectList.methods.createSubject(code,subject)
    .send({from:this.state.account})
    .once('receipt',(receipt)=>{
      this.setState({loading:false})
      this.loadBlockchainData()
    })
  }
  updateSubject(id,code,subject){
      // alert(id+name)
      this.setState({update:true})
      this.state.subjectList.methods.updatedSubject(id,code,subject)
      .send({from:this.state.account})
      .once('receipt',(receipt)=>{
        this.setState({loading:false})
        this.setState({update:false})
        this.loadBlockchainData()
      })
      this.setState({loading:false})
      this.setState({update:false})
      // alert(student._id)

  }
  markRetired(code) {
    this.setState({ loading: true });
    this.state.subjectList.methods.markRetired(code)
      .send({ from: this.state.account })
      .once("receipt", (receipt) => {
        this.setState({ loading: false });
        this.loadBlockchainData();
      });
  }


  //****MARKLIST*****/
  createMarks(studentid, subjectid, grades) {
    this.setState({loading:true})
    this.state.MarkList.methods.addMarks(studentid, subjectid, grades)
    .send({from: this.state.account})
    .once('receipt', (receipt) => {
      this.setState({loading:false})
      this.loadBlockchainData()
    })
  }
  findMarks(student_id, subject_id){
    this.setState({loading: true})
    return this.state.MarkList.methods
    .findMarks(student_id, subject_id)
    .call ({from: this.state.account})
  }

  
  render() {
    // const updateName=(event,student)=>{
    //   event.preventDefault()
    //   alert(student._id)

    // }
    return(
      <div className='container'>
        <h1>Students Marks Management System</h1>
        <h2>Add Students</h2>
        <p>Your account: {this.state.account}</p>
        <p>Total student : {this.state.studentCount}</p>
        <StudentList createStudent={this.createStudent} updateName={this.updateName}  update ={this.state.update} studentObj = {this.studentObj} />
        <ul id='studentlsit' className='list-unstyled'>
          {
            this.state.students.map((student, key) => {
              return(
                <li className='list-group-item checkbox' key={key}>
                  <span className='name alert'> {student._id} {student.cid} {student.name} </span>
                  <input className='form-check-input btn btn-success' type='checkbox' name={student._id} defaultChecked ={student.graduated} 
                  disabled={student.graduated} ref={(input) => {
                    this.checkbox = input
                  }}
                  onClick={(event) => {this.markGraduated(event.currentTarget.name)}}/>
                  <label className='form-check-label'>Graduated</label>
                  <button type="submit" className="btn btn-success" style={{ marginTop: '10px' }} onClick={()=>{this.setState({update:true})
                  // this.state.updateCID = student.cid
                  this.studentObj= student
                  }} >Update</button>
                  
                </li>

              )
            })
          }  
        </ul> 
        <h3>Add Subjects</h3>
        <p>Your account: {this.state.account}</p>
        <p>Total subject : {this.state.subjectCount}</p>
        <SubjectList createSubject={this.createSubject} updateSubject={this.updateSubject}  update ={this.state.update} subjectObj = {this.subjectObj} />
        <ul id='subjectList' className='list-unstyled'>
          {
            this.state.subjects.map((subject, key) => {
              return(
                <li className='list-group-item checkbox' key={key}>
                  <span className='name alert'> {subject._id} {subject.code} {subject.subject} </span>
                  <input className='form-check-input btn btn-success' type='checkbox' name={subject._id} defaultChecked ={subject.retired} 
                  disabled={subject.retired} ref={(input) => {
                    this.checkbox = input
                  }}
                  onClick={(event) => 
                  {this.markRetired(event.currentTarget.name)}
                  // {console.log(event.currentTarget.name)}
                }
                  />
                  <label className='form-check-label'>Retired</label>
                  <button type="submit" className="btn btn-success" style={{ marginTop: '10px' }} onClick={()=>{this.setState({update:true})
                  // this.state.updateCID = student.cid
                  this.subjectObj= subject
                  }} >Update</button>
                  
                </li>

              )
            })
          }  
        </ul> 
        <h3>Add Marks</h3>
        <p>Total marks records: {this.state.marksCount}</p>
        <MarkList subjects={this.state.subjects} students={this.state.students} createMarks={this.createMarks}/>
        
        <ul className='ul'  style={{  padding: '0px' }}>
    
          <FindMarks subjects={this.state.subjects} 
                     students={this.state.students} 
                     findMarks={this.findMarks}></FindMarks>
          
        </ul>
      </div>
    )
  }
}


export default App;

