import React, { Component } from 'react';

class MarkList extends Component {
  render() {
    const { students = [], subjects = [] } = this.props; // Set default values for students and subjects

    return (
      <form
        style={{
          borderRadius: '20px',
          padding: '20px',
          margin: '20px auto',
          width: '100%',
        }}
        className="p-4 border border-secondary"
        onSubmit={(event) => {
          event.preventDefault();
          this.props.createMarks(
            this.studentid.value,
            this.subjectid.value,
            this.grades.value
          );
        }}
      >
        <select
          id="studentid"
          className="form-select mb-2 border-secondary"
          style={{
            padding: '10px',
            margin: '10px auto',
            width: '100%',
            borderRadius: '10px',
          }}
          ref={(input) => {
            this.studentid = input;
          }}
        >
          <option defaultValue={true}>Select the student</option>
          {students.map((student, key) => (
            <option value={student.cid} key={key}>
              {student.cid} {student.name}
            </option>
          ))}
        </select>
        <select
          id="subjectid"
          className="form-select mb-2 border-secondary"
          style={{
            padding: '10px',
            margin: '10px auto',
            width: '100%',
            borderRadius: '10px',
          }}
          ref={(input) => {
            this.subjectid = input;
          }}
        >
          <option defaultValue={true}>Select the subject</option>
          {subjects.map((subject, key) => (
            <option value={subject.code} key={key}>
              {subject.code} {subject.name}
            </option>
          ))}
        </select>
        <select
          id="grades"
          className="form-select mb-2 border-secondary"
          style={{
            padding: '10px',
            margin: '10px auto',
            width: '100%',
            borderRadius: '10px',
          }}
          ref={(input) => {
            this.grades = input;
          }}
        >
          <option defaultValue={true}>Select the grade</option>
          <option value="1">A</option>
          <option value="2">B</option>
          <option value="3">C</option>
          <option value="4">D</option>
          <option value="5">F</option>
        </select>
        <input className="form-control btn btn-success" type="submit" hidden="" />
      </form>
    );
  }
}

export default MarkList;
