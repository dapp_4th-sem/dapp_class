var StudentList = artifacts.require("./StudentList.sol");
var SubjectList = artifacts.require("./SubjectList.sol");
var MarksList = artifacts.require("./MarksList.sol");
module.exports = function(deployer){
    deployer.deploy(StudentList);
    deployer.deploy(SubjectList);
    deployer.deploy(MarksList);
};
