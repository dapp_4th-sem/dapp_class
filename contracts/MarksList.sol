// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

contract MarksList{
    uint256 public marksCount = 0;
    enum Grades{A,B,C,D,FAIL}

    struct Marks{
        uint cid;
        string code;
        Grades grades;
    }

    mapping(uint => mapping(string => Marks))public marks;

    event addMarksEvent(
        uint indexed cid,
        string indexed code,
        Grades grades
    );

    constructor() {
        //addMarks(1001, "CSC101", 1)
    }
    function addMarks(uint _cid, string memory _code, Grades _grades) public {
        //require that Marks with this cid is not added before
        require(bytes(marks[_cid][_code].code).length == 0, "Marks not found");
        marksCount++;
        marks[_cid][_code] = Marks(_cid, _code, _grades);
        emit addMarksEvent(_cid, _code, _grades);
        
    }
    //fetch Marks
    function findMarks(uint _cid, string memory _code) public view returns (Marks memory){
        return marks[_cid][_code];
    }

  //update marks
    function updateMarks(uint _cid, string memory _code, Grades _newgrade) public returns (Marks memory) {
    require(bytes(marks[_cid][_code].code).length == 0 || marks[_cid][_code].grades == Grades.FAIL, "Marks already exist");
    marks[_cid][_code].grades = _newgrade;
    emit updateMarksEvent(_cid, _code, _newgrade);
    return marks[_cid][_code];
    }
     event updateMarksEvent(
        uint _cid,
        string  _code,
        Grades _newgrades
        );
}